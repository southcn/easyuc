<?php

namespace SouthCN\EasyUC\Repositories;

use Abel\PrivateApi\PrivateApi;
use AbelHalo\ApiProxy\ApiProxy;
use Illuminate\Support\Facades\Config;
use SouthCN\EasyUC\Exceptions\ApiFailedException;
use SouthCN\EasyUC\Service;

class UserCenterAPI
{
    protected $proxy;

    public function __construct()
    {
        $this->proxy = (new ApiProxy)->returnAsObject();

        Config::set('private-api._', ['return_type' => 'object']);
        Config::set('private-api.easyuc', [
            'app' => env('UC_APP'),
            'ticket' => env('UC_TICKET'),

            'sync-site-list' => ['url' => config('easyuc.oauth.base_url') . '/api/private/sync/site/list'],
        ]);
    }

    /**
     * 用户中心「获取用户详细信息」接口
     *
     * @return object
     * @throws ApiFailedException
     */
    public function getUserDetail(string $accessToken)
    {
        $url = config('easyuc.oauth.base_url') . '/api/oauth/user/detail';

        /** @var object $response */
        $response = $this->proxy->post($url, [
            'access_token' => $accessToken,
            'site_app_id' => config('easyuc.site_app_id'),
            'service_area_ids' => null,
        ]);

        if (empty($response->data)) {
            throw new ApiFailedException("调用 $url 接口失败");
        }

        return $response->data;
    }

    /**
     * 用户中心「获取站点列表」接口
     */
    public function getSiteList(): array
    {
        $response = PrivateApi::app('easyuc')->api('sync-site-list', [
            'site_app_id' => config('easyuc.site_app_id'),
        ]);

        return $response->data->list;
    }

    /**
     * 用户中心「统一登出」接口
     *
     * @throws ApiFailedException
     */
    public function logout(): void
    {
        $url   = config('easyuc.oauth.logout_url');
        $token = Service::token()->logout;

        // 被动登出情景下，无需再向用户中心通知登出
        if (Service::logoutSignal()->check()) {
            Service::logoutSignal()->clear();
            return;
        }

        /** @var object $response */
        $response = $this->proxy->post($url, [
            'logout_token' => $token,
        ]);

        if (0 !== $response->errcode) {
            throw new ApiFailedException("调用 $url 接口失败：{$response->errmessage}（Token={$token}）");
        }
    }
}
